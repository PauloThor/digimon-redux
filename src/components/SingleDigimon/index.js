import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import showDigimonThunk from "../../store/modules/singleDigimon/thunk";

const SingleDigimon = () => {
  const [name, setName] = useState("");

  const singleDigimon = useSelector((state) => state.singleDigimon);

  const dispatch = useDispatch();

  const handleShow = (name) => {
    dispatch(showDigimonThunk(name));
    setName("");
  };

  return (
    <div>
      <button onClick={() => handleShow(name)}>Escolha 1 digimon</button>
      <input onChange={(e) => setName(e.target.value)} value={name}></input>
      {singleDigimon && (
        <div>
          <p>{singleDigimon.singleDigimon.name}</p>
          <img src={singleDigimon.singleDigimon.img} alt={"digimon"} />
        </div>
      )}
    </div>
  );
};

export default SingleDigimon;
