import { useSelector } from "react-redux";

const Digimons = () => {
  const { digimons } = useSelector((state) => state);

  return (
    <div>
      <ul style={{ listStyle: "none", display: "flex", flexWrap: "wrap" }}>
        {digimons.map((digimon, i) => (
          <li key={i} style={{ margin: "10px", border: "2px solid red" }}>
            <p>{digimon.digimon.name}</p>
            <img src={digimon.digimon.img} alt={digimon.digimon.name} />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Digimons;
