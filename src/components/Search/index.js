import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import addDigimonsTunk from "../../store/modules/digimons/thunk";

const Search = () => {
  const [input, setInput] = useState("");

  const dispatch = useDispatch();

  const digimons = useSelector((state) => state.digimons);

  const handleSearch = () => {
    console.log("digimons:", digimons);
    dispatch(addDigimonsTunk(input));
    setInput("");
  };

  return (
    <div>
      <h2>Procure pelo seu Digimon!</h2>
      <div>
        <input
          value={input}
          onChange={(event) => setInput(event.target.value)}
          placeholder="Procure seu Digimon"
        ></input>
        <button onClick={handleSearch}>Pesquisar</button>
      </div>
    </div>
  );
};

export default Search;
