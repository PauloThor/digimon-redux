import axios from "axios";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import getDigimonTunk from "../../store/modules/digimonsList/thunk";

const DigimonList = () => {
  const [list, setList] = useState([]);
  const [show, setShow] = useState(false);

  const digimonList = useSelector((state) => state.digimonList.digimonList);
  const dispatch = useDispatch();

  const handleShow = (list) => {
    dispatch(getDigimonTunk(list));
    setShow(!show);
    console.log(digimonList);
  };

  useEffect(() => {
    axios.get(`https://digimon-api.vercel.app/api/digimon`).then((res) => {
      setList(res.data);
    });
  }, []);

  return (
    <div>
      <button onClick={() => handleShow(list)}>Todos</button>
      <ul style={{ listStyle: "none", display: "flex", flexWrap: "wrap" }}>
        {show &&
          digimonList.map((digimon) => (
            <li
              key={digimon.name}
              style={{ margin: "10px", border: "2px solid grey" }}
            >
              <p>{digimon.name}</p>
              <img src={digimon.img} alt={digimon.name} />
            </li>
          ))}
      </ul>
    </div>
  );
};

export default DigimonList;
