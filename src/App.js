import "./App.css";
import Search from "./components/Search";
import Digimons from "./components/Digimons";
import DigimonList from "./components/DigimonList";
import SingleDigimon from "./components/SingleDigimon";
import { Route, Switch } from "react-router";

function App() {
  return (
    <Switch>
      <Route exact path="/entrega-base">
        <Search />
        <Digimons />
      </Route>
      <Route exatch path="/extra-lista">
        <DigimonList />
      </Route>
      <Route exact path="/extra-digimon-unico">
        <SingleDigimon />
      </Route>
    </Switch>
  );
}

export default App;
