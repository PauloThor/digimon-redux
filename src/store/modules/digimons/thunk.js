import axios from "axios";

import { addDigimon } from "./action";

const addDigimonThunk = (digimon, setError) => {
  return (dispatch) => {
    axios
      .get(`https://digimon-api.vercel.app/api/digimon/name/${digimon}`)
      .then((res) => {
        dispatch(addDigimon(...res.data));
        console.log(...res.data);
      })
      .catch((e) =>
        dispatch(
          addDigimon({
            name: "Digimon não encontrado",
            img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTSywYBSz0nhnxOsxLQBSfw8CdPiyv93-PjJg&usqp=CAU",
          })
        )
      );
  };
};

export default addDigimonThunk;
