import { ADD_DIGIMONS } from "./actionTypes";

const digimonsReducer = (state = [], action) => {
  switch (action.type) {
    case ADD_DIGIMONS:
      return [...state, action];

    default:
      return state;
  }
};

export default digimonsReducer;
