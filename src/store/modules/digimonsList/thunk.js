import { getDigimon } from "./action";

const getDigimonThunk = (list) => {
  return (dispatch) => {
    dispatch(getDigimon(list));
  };
};

export default getDigimonThunk;
