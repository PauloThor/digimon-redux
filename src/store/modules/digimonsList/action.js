import { GET_DIGIMONS } from "./actionTypes";

export const getDigimon = (digimonList) => ({
  type: GET_DIGIMONS,
  digimonList,
});
