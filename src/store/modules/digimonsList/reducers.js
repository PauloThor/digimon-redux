import { GET_DIGIMONS } from "./actionTypes";

const digimonListReducer = (state = [], action) => {
  switch (action.type) {
    case GET_DIGIMONS:
      return action;

    default:
      return state;
  }
};

export default digimonListReducer;
