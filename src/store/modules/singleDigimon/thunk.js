import axios from "axios";

import { showDigimon } from "./action";

const showDigimonThunk = (digimon) => {
  return (dispatch) => {
    axios
      .get(`https://digimon-api.vercel.app/api/digimon/name/${digimon}`)
      .then((res) => {
        dispatch(showDigimon(...res.data));
        // console.log(...res.data);
      })
      .catch((e) =>
        dispatch(
          showDigimon({
            name: "Digimon não encontrado",
            img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTSywYBSz0nhnxOsxLQBSfw8CdPiyv93-PjJg&usqp=CAU",
          })
        )
      );
  };
};

export default showDigimonThunk;
