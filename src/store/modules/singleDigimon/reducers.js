import { SHOW_DIGIMONS } from "./actionTypes";

const showReducer = (state = "", action) => {
  switch (action.type) {
    case SHOW_DIGIMONS:
      return action;

    default:
      return state;
  }
};

export default showReducer;
