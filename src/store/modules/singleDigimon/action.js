import { SHOW_DIGIMONS } from "./actionTypes";

export const showDigimon = (singleDigimon) => ({
  type: SHOW_DIGIMONS,
  singleDigimon,
});
