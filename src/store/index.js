import { createStore, combineReducers, applyMiddleware } from "redux";

import thunk from "redux-thunk";

import digimonsReducer from "./modules/digimons/reducers";
import digimonListReducer from "./modules/digimonsList/reducers";
import showReducer from "./modules/singleDigimon/reducers";

const reducers = combineReducers({
  digimons: digimonsReducer,
  digimonList: digimonListReducer,
  singleDigimon: showReducer,
});

const store = createStore(reducers, applyMiddleware(thunk));

export default store;
