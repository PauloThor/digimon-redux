import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import store from "./store";

import { Provider } from "react-redux";
import { BrowserRouter, Link } from "react-router-dom";

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Provider store={store}>
        <header style={{ display: "flex" }}>
          <div style={{ margin: "10px" }}>
            <Link to="/entrega-base">Entrega Base</Link>
          </div>
          <div style={{ margin: "10px" }}>
            <Link to="extra-lista">Extra Lista</Link>
          </div>
          <div style={{ margin: "10px" }}>
            <Link to="/extra-digimon-unico">Extra Digimon Único</Link>
          </div>
        </header>
        <div className="app">
          <App />
        </div>
      </Provider>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
